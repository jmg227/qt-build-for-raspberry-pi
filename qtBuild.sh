#!/bin/bash

# Script to make it easier to configure QT and cross-compile it

BUILDTYPE=${BUILDTYPE:=release}
OS_VERSION=${OS_VERSION:=10}
QTVERSION=${QTVERSION:=5.12.3}
QTDIR=${QTDIR:="${HOME}/opt/Qt/${QTVERSION}"}
WORKDIR=${WORKDIR:="{HOME}/raspi"}
QTBUILDDIR=${WORKDIR}/qtBuild/raspbian_${OS_VERSION}_Qt_${QTVERSION}
SYSROOT=${WORKDIR}/sysroot_${OS_VERSION}
# Linaro gcc 6.3
GCCPATH=${WORKDIR}/linaro/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf/bin
# Older cross compiler specified in qt Wiki
# GCCPATH=${WORKDIR}/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin

missing_dir()
{
  echo "Missing required directory: $1"
  if [ -n "$2" ] ; then
    echo "See: $2 for help."
  fi
}

## Process options
PITARGET=""

while getopts ":s:" $opt; do
  case ${opt} in
    s ) echo "sync sysroot"
      PITARGET=$OPTARG
      ;;
    \? ) echo "Invalid option: ${OPTARG}" 1>&2
      ;;
    : ) echo "Invalid option: ${OPTARG} would like to have an argument, please." 1>&2
      ;;
  esac
done
shift $((OPTIND -1))

cd ${WORKDIR}

if [ ! -d tools ] ; then
  git clone https://github.com/raspberrypi/tools
fi

if [ -n "${PITARGET}" ] ; then
  if [ ! -d ${SYSROOT}/usr ] ; then
    mkdir -p ${SYSROOT}/usr
  fi
  if [ ! -d ${SYSROOT}/opt ] ; then
    mkdir -p ${SYSROOT}/opt
  fi
  rsync -avz pi@${PITARGET}.local:/lib ${SYSROOT}
  rsync -avz pi@${PITARGET}.local:/usr/include ${SYSROOT}/usr
  rsync -avz pi@${PITARGET}.local:/usr/lib ${SYSROOT}/usr
  rsync -avz pi@${PITARGET}.local:/opt/vc ${SYSROOT}/opt
fi

if [ ! -f sysroot-relativelinks.py ] ; then
  wget https://raw.githubusercontent.com/Kukkimonsuta/rpi-buildqt/master/scripts/utils/sysroot-relativelinks.py
  chmod +x sysroot-relativelinks.py
  ./sysroot-relativelinks.py ${SYSROOT}
fi

SYSROOTSUBDIRS=(usr lib opt)
for SUBDIR in "${SYSROOTSUBDIRS[@]}" ; do
  if [ ! -d "${SYSROOT}/${SUBDIR}" ] ; then
    missing_dir "${SYSROOT}/${SUBDIR}" "https://wiki.qt.io/RaspberryPi2EGLFS"
  fi
  exit 1
done

if [ -d "${QTBUILDDIR}" ] ; then
  if [ -w " ${QTBUILDDIR}" ] ; then
	  rm -rf ${QTBUILDDIR}
  else
	  sudo rm -rf ${QTBUILDDIR}
  fi
fi

mkdir -p ${QTBUILDDIR}/qtbase

if [ ! -d "${WORKDIR}/rpi_${OS_VERSION}/qt5pi" ] ; then
  mkdir -p ${WORKDIR}/rpi_${OS_VERSION}/qt5pi
fi

if [ ! -d "${WORKDIR}/rpi_${OS_VERSION}/qt5" ] ; then
  mkdir -p ${WORKDIR}/rpi_${OS_VERSION}/qt5
fi

echo ">>>>>>> Configuring Qt build <<<<<<<<<<<"
cd ${QTBUILDDIR}/qtbase

${QTDIR}/Src/qtbase/configure -${BUILDTYPE} -opengl es2 -device linux-rasp-pi-g++ -device-option CROSS_COMPILE=${GCCPATH}/arm-linux-gnueabihf- -sysroot ${SYSROOT} -opensource -confirm-license -skip qtwayland -skip qtlocation -skip qtscript -no-feature-opengles3 -make libs -prefix /usr/local/qt5pi -extprefix ${WORKDIR}/rpi_${OS_VERSION}/qt5pi -hostprefix ${WORKDIR}/rpi_${OS_VERSION}/qt5 -no-use-gold-linker -v

echo ">>>>>>>> Building qtbase <<<<<<<<<<<<<<<"
make -j4

echo ">>>>>>>> Installing qtbase <<<<<<<<<<<<<"
make install

if [ -z "$QTMODULES" ] ; then
  QTMODULES=(qt3d qtserialport qtserialbus qtcharts qtimageformats qtxmlpatterns)
fi

for MODULE in "${QTMODULES[@]}" ; do
  if [ -d ${QTDIR}/Src/${MODULE} ] ; then
    mkdir ${QTBUILDDIR}/${MODULE}
    cd ${QTBUILDDIR}/${MODULE}
    echo ">>>>>>>> Configuring ${MODULE}  <<<<<<<<<<<<<<<"
    /srv/srcs/rpi_${OS_VERSION}/qt5/bin/qmake ${QTDIR}/Src/${MODULE}
    echo ">>>>>>>> Building    ${MODULE}  <<<<<<<<<<<<<<<"
    make -j4
    echo ">>>>>>>> Installing  ${MODULE}  <<<<<<<<<<<<<<<"
    make install
  else
    echo "!!!!!!!! No source for module ${MODULE} !!!!!!!"
    echo " ... Skipping ..."
  fi
done
