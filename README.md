# qt-build-for-raspberry-pi

This project consists of scripts to cross-compile Qt for the Raspberry Pi according to the instructions at https://wiki.qt.io/RaspberryPi2EGLFS.  At this point, the scripts do not automate the entire process.  The following pre-work must be done:

 1. Install the packages on the Raspberry Pi that are required to build the sysroot image.
 2. *(Optional)* Install a more recent cross-compiler than the one provided in the Raspberry Pi tools repo.  The script defaults to using Linaro 6.3.1 which is installed in `${WORKDIR}/linaro/gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf/bin`.  Either install this cross-compiler at this location or export a `GCCPATH` environment variable to specify the location of your cross-compiler.  The path for the older compiler that is in the Raspberry Pi tools repo is in the shell script comments.
